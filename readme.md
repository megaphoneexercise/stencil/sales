# README

To run:

* Clone this repo
* `npm install`
* `npm start`

My crud resource (`src/http_services/crud.http_service.ts`) is pointed at localhost:3000. If you serve your Rails server on a different port you will need to adjust the port number here.