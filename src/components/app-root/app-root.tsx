import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root'
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={true}>
          <ion-route url="/" component="sales-page" />
          <ion-route url="/errors/server" component="errors-server" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
