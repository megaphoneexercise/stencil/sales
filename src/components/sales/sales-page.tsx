import {Component, h, Method, Element, State} from '@stencil/core';
import {CsvsHttpService} from "../../http_services/csvs.http_service";
import Papa from 'papaparse';
import {SaleHttpService} from "../../http_services/sale.http_service";
import {UtilsService} from "../../services/utils.service";

@Component({
  tag: 'sales-page'
})
export class SalesPage {

  @Element() el: Element;
  @State() sales: any;
  @State() total = 0;
  data: any;

  async componentWillLoad() {
    const response = await new SaleHttpService().query({});
    this.sales = response;
    this.calculateTotal();
  }

  @Method()
  async selectFiles() {
    const fileInput = this.el.querySelector('#fileInput');
    (fileInput as any).click();
  }

  async parseData(reader) {
    const base64 = (reader.result as string).split('64,')[1];
    const decoded = atob(base64);
    const data = Papa.parse(decoded, {
      header: true,
      dynamicTyping: true
    });
    const filtered = data.data.filter(obj => obj['Customer Name'] !== null);
    filtered.map(async (order) => {
      await new SaleHttpService().post( {
        customer_name: order['Customer Name'],
        item_description: order['Item Description'],
        item_price: order['Item Price'],
        merchant_address: order['Merchant Address'],
        merchant_name: order['Merchant Name'],
        quantity: order['Quantity']
      })
    });
    const response = await new SaleHttpService().query({});
    this.sales = UtilsService.deepClone(response)
  }

  async upload(event) {
    const files = event.target.files;
    const file = files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = async () => {
      await this.parseData(reader);
      const csv = await new CsvsHttpService().post({
        original_file: reader.result,
        file_type: file.type,
      });
      console.log(csv)
    };
  }

  calculateTotal() {
    this.sales.map(sale => {
      this.total += (sale.item_price * sale.quantity)
    })
  }

  renderTotal() {
    return [
      <ion-item lines='none'>
        <ion-text slot='end'>
          Total: {this.total.toFixed(2)}
        </ion-text>
      </ion-item>
    ]
  }

  renderUploadButton() {
    return [
      <input
          id='fileInput'
          hidden={true}
          multiple={false}
          name='file'
          type='file'
          onChange={(event) => this.upload(event)}
          accept='.csv'/>,
      <ion-fab vertical="bottom" horizontal="start" slot="fixed">
        <ion-fab-button onClick={() => this.selectFiles()}>
          <ion-icon name="cloud-upload-outline" />
        </ion-fab-button>
      </ion-fab>
    ]
  }

  renderSales() {
    return this.sales.map(sale => {
      return [
        <ion-item>
          <ion-col>
            {sale.customer_name}
          </ion-col>
          <ion-col>
            {sale.item_description}
          </ion-col>
          <ion-col>
            {sale.item_price}
          </ion-col>
          <ion-col>
            {sale.quantity}
          </ion-col>
          <ion-col>
            {sale.merchant_name}
          </ion-col>
          <ion-col>
            {sale.merchant_address}
          </ion-col>
        </ion-item>
      ]
    })
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-title>Sales</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-item>
        <ion-col>
          Customer Name
        </ion-col>
        <ion-col>
          Item Description
        </ion-col>
        <ion-col>
          Item Price
        </ion-col>
        <ion-col>
          Quantity
        </ion-col>
        <ion-col>
          Merchant Name
        </ion-col>
        <ion-col>
          Merchant Address
        </ion-col>
      </ion-item>,
      <ion-content>
        {this.renderSales()}
        {this.renderTotal()}
      </ion-content>,
      this.renderUploadButton()
    ];
  }
}
