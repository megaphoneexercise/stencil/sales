import {CrudHttpService} from './crud.http_service';

export class SaleHttpService extends CrudHttpService {
  constructor() {
    super('sales');
  }
}
