import {CrudHttpService} from './crud.http_service';

export class CsvsHttpService extends CrudHttpService {
  constructor() {
    super('csvs');
  }
}
