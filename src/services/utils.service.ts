export class UtilsService {
  static deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  static clamp(num: number, min: number, max: number) {
    return num < min ? min : num > max ? max : num;
  }

  static setDateValue(destination: Date, source: Date) {
    const updated = new Date(destination);
    updated.setFullYear(source.getFullYear(), source.getMonth(), source.getDate());
    return updated;
  }
}
