export class SearchService {
  arr: any[];
  filterTerms: string[];
  constructor() {
  }

  static search(haystack: any[], needle: string = '', filterTerms?: string[]) {
    if (filterTerms === undefined) {
      filterTerms = Object.keys(haystack[0]);
    }
    if ([undefined, null].includes(needle)) { needle = ''; }
    if ('' === needle.trim()) { return [...haystack]; }
    return [...haystack].filter((item: any) => {
      const matches = filterTerms.map((key) => {
        const [key1, key2, key3] = key.split('.');
        let term = '';
        if (key3 !== undefined) {
          term = item[key1][key2][key3];
        } else if (key2 !== undefined) {
          term = item[key1][key2];
        } else {
          term = item[key1];
        }
        return SearchService.match(needle, term);
      });

      return matches.includes(true);
    });
  }

  static match(searchTerm, term) {
    const matches = searchTerm.split(/\s+/).filter((searchTerm) => {
      return searchTerm.trim() !== '';
    }).map((searchTerm) => {
      return term.toLowerCase().includes(searchTerm);
    });
    return !matches.includes(false);
  }
}
